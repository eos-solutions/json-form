import React from 'react'
import Json from '../components/Json'
import Form from '../components/Form'

const Home = () => {
  return (
    <header className='container'>
      <h1>Form to Json</h1>
      <h3 className='subheadline'>
        Simply enter the information present in the form, we will translate that
        into Json format.
      </h3>
      <div className='flex flex-wrap'>
        <Form />
        <Json />
      </div>
    </header>
  )
}

export default Home
