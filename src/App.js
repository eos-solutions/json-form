import React from 'react'
import { Router } from '@reach/router'
import './assets/scss/index.scss'
import Home from './pages/Home'
import StoreProvider from './utils/Store'

function App() {
  return (
    <StoreProvider>
      <header>
        <div className='container'>
          <img
            src={require('./assets/images/eos.png')}
            alt='EOS'
            className='page-logo'
          />
        </div>
      </header>
      <Router primary={false}>
        <Home path={`${process.env.PUBLIC_URL}/`} />
      </Router>
    </StoreProvider>
  )
}

export default App
