import React, { createContext, useReducer } from 'react'

export const StoreContextState = createContext()
export const StoreContextDispatch = createContext()

const initialState = {
  id: new Date().valueOf(),
  name: '',
  role: '',
  location: '',
  quote: '',
  img: '',
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'addName':
      return { ...state, name: action.payload }
    case 'addRole':
      return { ...state, role: action.payload }
    case 'addLocation':
      return { ...state, location: action.payload }
    case 'addQuote':
      return { ...state, quote: action.payload }
    case 'addImg':
      return { ...state, img: action.payload }
    default:
      throw new Error('no action defined')
  }
}

const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState)
  return (
    <StoreContextState.Provider value={state}>
      <StoreContextDispatch.Provider value={dispatch}>
        {children}
      </StoreContextDispatch.Provider>
    </StoreContextState.Provider>
  )
}

export default StoreProvider
