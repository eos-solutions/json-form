import React, { useContext, useState } from 'react'
import { StoreContextDispatch } from '../utils/Store'
import { TextField } from '@material-ui/core'

const Form = () => {
  const dispatch = useContext(StoreContextDispatch)
  const [stateTextarea, setStateTextarea] = useState(false)

  return (
    <div className='flex-content form-container'>
      <h2>Enter your data to convert it</h2>
      <TextField
        type='text'
        variant='standard'
        label='Full name'
        onChange={(val) => {
          dispatch({ type: 'addName', payload: val.target.value })
        }}
      />
      <TextField
        type='text'
        variant='standard'
        label='Your role'
        onChange={(val) => {
          dispatch({ type: 'addRole', payload: val.target.value })
        }}
      />
      <TextField
        fullWidth
        type='text'
        variant='standard'
        label='Image file name (no spaces. Add file extension)'
        onChange={(val) => {
          dispatch({ type: 'addImg', payload: val.target.value })
        }}
      />
      <TextField
        type='text'
        variant='standard'
        label='Your location'
        onChange={(val) => {
          dispatch({ type: 'addLocation', payload: val.target.value })
        }}
      />
      <TextField
        type='text'
        variant='standard'
        label='What do you love about SUSE'
        fullWidth
        multiline
        rowsMax={4}
        error={stateTextarea}
        helperText='Max 300 characters'
        onChange={(val) => {
          setStateTextarea(val.target.value.length > 300 ? true : false)
          dispatch({ type: 'addQuote', payload: val.target.value })
        }}
      />
    </div>
  )
}

export default Form
